CREATE DATABASE IF NOT EXISTS lndc;
USE lndc;

CREATE TABLE IF NOT EXISTS role (
    id INT NOT NULL AUTO_INCREMENT,
    uuid VARCHAR(255) NOT NULL,
    name VARCHAR(255) NOT NULL,
    description  VARCHAR(255) NOT NULL,
    created_date DATE NOT NULL,
    PRIMARY KEY (id),
    UNIQUE (uuid),
    UNIQUE (name)
) ENGINE=InnoDB CHARSET=utf8;

CREATE TABLE IF NOT EXISTS userEntity
(
    id            INT          NOT NULL AUTO_INCREMENT,
    uuid          VARCHAR(255) NOT NULL,
    password_hash VARCHAR(255) NOT NULL,
    role_id       INT          NOT NULL,
    username      VARCHAR(255) NOT NULL,
    first_name    VARCHAR(255),
    last_name     VARCHAR(255),
    email         VARCHAR(255) NOT NULL,
    phone         VARCHAR(255),
    university    VARCHAR(255),
    city          VARCHAR(255),
    hobbies       VARCHAR(255),
    created_date  DATE         NOT NULL,
    PRIMARY KEY (id),
    UNIQUE (uuid),
    UNIQUE (username),
    UNIQUE (email),
    FOREIGN KEY (role_id) REFERENCES role (id)
) ENGINE = InnoDB
  CHARSET = utf8;


insert into lndc.role (id, uuid, name, description, created_date)
VALUES (1, '319ceff5-c07e-4883-bc56-43d9efd38dcb', 'ROLE_USER', 'USER', '2020-05-06');
insert into lndc.role (id, uuid, name, description, created_date)
VALUES (2, 'ba2e9d55-7ff8-4223-8c77-24c0233b3de9', 'ROLE_SPONSOR', 'SPONSOR', '2020-05-06');
insert into lndc.role (id, uuid, name, description, created_date)
VALUES (3, '0724b423-b44b-4134-8aee-999a867153e0', 'ROLE_ORGANISATOR', 'ORGANISATOR', '2020-05-06');
insert into lndc.role (id, uuid, name, description, created_date)
VALUES (4, 'cea85e9e-8c07-4fdd-b144-ddc76108fd1c', 'ROLE_ADMIN', 'ADMIN', '2020-05-06');
