package com.lndc.website.backend.services;

import com.lndc.website.backend.models.ERole;
import com.lndc.website.backend.models.RoleEntity;
import com.lndc.website.backend.models.UserEntity;
import com.lndc.website.backend.repositories.RoleRepository;
import com.lndc.website.backend.repositories.UserRepository;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.argon2.Argon2PasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest
class UserServiceTest {
		@Autowired
		RoleRepository roleRepository;
		@Autowired
		Argon2PasswordEncoder encoder;
		String
				email = "user@email.com",
				username = "username";
		@Autowired
		private UserRepository userRepository;
		@Autowired
		private UserService userService;
		@Autowired
		private RoleService roleService;

		@Test
		void findUserByEmail() {
				UserEntity user = new UserEntity(username, email, encoder.encode("password"));
				RoleEntity role = roleService.findByName(ERole.ROLE_USER).get();
				user.setURoleId(role);

				if (!userService.existsByEmail(email))
						userRepository.save(user);


				UserEntity user1 = userRepository.findByEmail(email);

				assertThat(user1.getEmail()).isNotNull();
				assertThat(user1.getEmail()).isEqualTo(email);
				assertThat(userService.existsByEmail(email)).isEqualTo(true);


				userService.deleteById(userRepository.findByEmail(email).getId());
		}

		@Test
		void findUserByUserName() {
				UserEntity user = new UserEntity(username, email, encoder.encode("password"));
				RoleEntity role = roleService.findByName(ERole.ROLE_ORGANISATOR).get();
				user.setURoleId(role);

				if (!userService.existsByEmail(email))
						userRepository.save(user);

				assertThat(userService.findUserByUserName(username).get().getEmail()).isEqualTo(email);
				assertThat(userService.findUserByUserName(username).get().getUsername()).isEqualTo(username);

				assertTrue(encoder.matches("password", userService.findUserByUserName(username).get().getPasswordHash()));
				assertThat(userService.findUserByUserName(username).get().getUsername()).isNotNull();
				assertThat(userService.existsByUsername(username)).isEqualTo(true);


				assertThat(userService.findUserByUserName(username).get().getURoleId().getName()).isEqualTo(ERole.ROLE_ORGANISATOR);


				userService.deleteById(userRepository.findByEmail(email).getId());
		}
}
