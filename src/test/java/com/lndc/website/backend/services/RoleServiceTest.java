package com.lndc.website.backend.services;

import com.lndc.website.backend.models.ERole;
import com.lndc.website.backend.models.RoleEntity;
import com.lndc.website.backend.repositories.RoleRepository;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
class RoleServiceTest {

		@Autowired
		private RoleRepository roleRepository;

		@Autowired
		private RoleService roleService;

		@Test
		void findByName() {

				RoleEntity roleAdmin = roleRepository.findRoleByName(ERole.ROLE_ADMIN);
				Optional<RoleEntity> roleA = roleService.findByName(ERole.ROLE_ADMIN);

				RoleEntity roleSpon = roleRepository.findRoleByName(ERole.ROLE_SPONSOR);
				Optional<RoleEntity> roleS = roleService.findByName(ERole.ROLE_SPONSOR);

				RoleEntity roleOrg = roleRepository.findRoleByName(ERole.ROLE_ORGANISATOR);
				Optional<RoleEntity> roleO = roleService.findByName(ERole.ROLE_ORGANISATOR);

				RoleEntity roleUser = roleRepository.findRoleByName(ERole.ROLE_USER);
				Optional<RoleEntity> roleU = roleService.findByName(ERole.ROLE_USER);

				assertThat(roleAdmin.getName()).isEqualTo(roleA.get().getName());
				assertThat(roleSpon.getName()).isEqualTo(roleS.get().getName());
				assertThat(roleOrg.getName()).isEqualTo(roleO.get().getName());
				assertThat(roleUser.getName()).isEqualTo(roleU.get().getName());
		}

		@Test
		void findById() {
				RoleEntity roleAdmin = roleRepository.findById(4L);
				RoleEntity roleA = roleService.findRoleById(4L);

				RoleEntity roleSpon = roleRepository.findById(2L);
				RoleEntity roleS = roleService.findRoleById(2L);

				RoleEntity roleOrg = roleRepository.findById(3L);
				RoleEntity roleO = roleService.findRoleById(3L);

				RoleEntity roleUser = roleRepository.findById(1L);
				RoleEntity roleU = roleService.findRoleById(1L);

				assertThat(roleAdmin.getId()).isEqualTo(roleA.getId());
				assertThat(roleSpon.getId()).isEqualTo(roleS.getId());
				assertThat(roleOrg.getId()).isEqualTo(roleO.getId());
				assertThat(roleUser.getId()).isEqualTo(roleU.getId());
		}
}
