package com.lndc.website.backend.models;

public enum ERole {
		ROLE_USER,
		ROLE_SPONSOR,
		ROLE_ORGANISATOR,
		ROLE_ADMIN
}
