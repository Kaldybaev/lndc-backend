package com.lndc.website.backend.models;

import javax.persistence.*;
import java.sql.Date;
import java.util.Objects;

@Entity
@Table(name = "role", uniqueConstraints = {
		@UniqueConstraint(columnNames = "name"),
		@UniqueConstraint(columnNames = "uuid")
})
public class RoleEntity {

		private long Id;
		private String uuid;
		private ERole name;
		private String description;
		private Date createdDate;

		@Id
		@GeneratedValue(strategy = GenerationType.IDENTITY)
		@Column(name = "id")
		public long getId() {
				return Id;
		}

		public void setId(long roleId) {
				this.Id = roleId;
		}

		@Basic
		@Column(name = "uuid")
		public String getUuid() {
				return uuid;
		}

		public void setUuid(String uuid) {
				this.uuid = uuid;
		}

		@Basic
		@Enumerated(EnumType.STRING)
		@Column(name = "name")
		public ERole getName() {
				return name;
		}

		public void setName(ERole name) {
				this.name = name;
		}

		@Basic
		@Column(name = "description")
		public String getDescription() {
				return description;
		}

		public void setDescription(String description) {
				this.description = description;
		}

		@Basic
		@Column(name = "created_date")
		public Date getCreatedDate() {
				return createdDate;
		}

		public void setCreatedDate(Date createdDate) {
				this.createdDate = createdDate;
		}

		@Override
		public boolean equals(Object o) {
				if (this == o) return true;
				if (o == null || getClass() != o.getClass()) return false;
				RoleEntity that = (RoleEntity) o;
				return Id == that.Id &&
						Objects.equals(uuid, that.uuid) &&
						Objects.equals(name, that.name) &&
						Objects.equals(description, that.description) &&
						Objects.equals(createdDate, that.createdDate);
		}

		@Override
		public int hashCode() {
				return Objects.hash(Id, uuid, name, description, createdDate);
		}
}
