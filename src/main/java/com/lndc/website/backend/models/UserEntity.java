package com.lndc.website.backend.models;

import javax.persistence.*;
import java.sql.Date;
import java.util.Objects;

@Entity
@Table(name = "user", uniqueConstraints = {
		@UniqueConstraint(columnNames = "username"),
		@UniqueConstraint(columnNames = "email"),
		@UniqueConstraint(columnNames = "uuid")
})
public class UserEntity {
		private Long id;
		private String uuid;
		private String passwordHash;
		private String passwordSalt;
		private String username;
		private String firstName;
		private String lastName;
		private String email;
		private String phone;
		private String university;
		private String city;
		private String hobbies;
		private Date createdDate;
		private RoleEntity uRoleId;

		public UserEntity() {
		}

		public UserEntity(String username, String email, String passwordHash) {
				this.username = username;
				this.email = email;
				this.passwordHash = passwordHash;
		}

		@Id
		@GeneratedValue(strategy = GenerationType.IDENTITY)
		@Column(name = "id")
		public Long getId() {
				return id;
		}

		public void setId(Long id) {
				this.id = id;
		}

		@Basic
		@Column(name = "uuid")
		public String getUuid() {
				return uuid;
		}

		public void setUuid(String uuid) {
				this.uuid = uuid;
		}

		@Basic
		@Column(name = "password_hash")
		public String getPasswordHash() {
				return passwordHash;
		}

		public void setPasswordHash(String passwordHash) {
				this.passwordHash = passwordHash;
		}

		@Basic
		@Column(name = "password_salt")
		public String getPasswordSalt() {
				return passwordSalt;
		}

		public void setPasswordSalt(String passwordSalt) {
				this.passwordSalt = passwordSalt;
		}

		@Basic
		@Column(name = "username")
		public String getUsername() {
				return username;
		}

		public void setUsername(String username) {
				this.username = username;
		}

		@Basic
		@Column(name = "first_name")
		public String getFirstName() {
				return firstName;
		}

		public void setFirstName(String firstName) {
				this.firstName = firstName;
		}

		@Basic
		@Column(name = "last_name")
		public String getLastName() {
				return lastName;
		}

		public void setLastName(String lastName) {
				this.lastName = lastName;
		}

		@Basic
		@Column(name = "email")
		public String getEmail() {
				return email;
		}

		public void setEmail(String email) {
				this.email = email;
		}

		@Basic
		@Column(name = "phone")
		public String getPhone() {
				return phone;
		}

		public void setPhone(String phone) {
				this.phone = phone;
		}

		@Basic
		@Column(name = "university")
		public String getUniversity() {
				return university;
		}

		public void setUniversity(String university) {
				this.university = university;
		}

		@Basic
		@Column(name = "city")
		public String getCity() {
				return city;
		}

		public void setCity(String city) {
				this.city = city;
		}

		@Basic
		@Column(name = "hobbies")
		public String getHobbies() {
				return hobbies;
		}

		public void setHobbies(String hobbies) {
				this.hobbies = hobbies;
		}

		@Basic
		@Column(name = "created_date")
		public Date getCreatedDate() {
				return createdDate;
		}

		public void setCreatedDate(Date createdDate) {
				this.createdDate = createdDate;
		}

		@Override
		public boolean equals(Object o) {
				if (this == o) return true;
				if (o == null || getClass() != o.getClass()) return false;
				UserEntity that = (UserEntity) o;
				return Objects.equals(id, that.id) &&
						Objects.equals(uuid, that.uuid) &&
						Objects.equals(passwordHash, that.passwordHash) &&
						Objects.equals(passwordSalt, that.passwordSalt) &&
						Objects.equals(username, that.username) &&
						Objects.equals(firstName, that.firstName) &&
						Objects.equals(lastName, that.lastName) &&
						Objects.equals(email, that.email) &&
						Objects.equals(phone, that.phone) &&
						Objects.equals(university, that.university) &&
						Objects.equals(city, that.city) &&
						Objects.equals(hobbies, that.hobbies) &&
						Objects.equals(createdDate, that.createdDate);
		}

		@Override
		public int hashCode() {
				return Objects.hash(id, uuid, passwordHash, passwordSalt, username, firstName, lastName, email, phone, university, city, hobbies, createdDate);
		}

		@OneToOne(fetch = FetchType.LAZY)
		@JoinColumn(name = "u_role_id", referencedColumnName = "id")
		public RoleEntity getURoleId() {
				return uRoleId;
		}

		public void setURoleId(RoleEntity roleId) {
				this.uRoleId = roleId;
		}

}
