package com.lndc.website.backend.services;

import com.lndc.website.backend.models.ERole;
import com.lndc.website.backend.models.RoleEntity;
import com.lndc.website.backend.repositories.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class RoleService {
		@Autowired
		private RoleRepository roleRepository;

		public Optional<RoleEntity> findByName(ERole name) {
				return roleRepository.findByName(name);
		}

		public RoleEntity findRoleById(long id) {
				return roleRepository.findById(id);
		}

		public RoleEntity findRoleByName(ERole name) {
				return roleRepository.findRoleByName(name);
		}


}
