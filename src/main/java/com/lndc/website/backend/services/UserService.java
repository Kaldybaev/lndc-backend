package com.lndc.website.backend.services;

import com.lndc.website.backend.models.UserEntity;
import com.lndc.website.backend.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {
		@Autowired
		private UserRepository userRepository;

		public UserEntity findUserByEmail(String email) {
				return userRepository.findByEmail(email);
		}

		public Optional<UserEntity> findUserByUserName(String userName) {
				return userRepository.findByUsername(userName);
		}

		public void save(UserEntity userEntity) {
				this.userRepository.save(userEntity);
		}

		public boolean isVerified(String email) {
				return this.userRepository.existsByEmail(email);
		}

		public boolean existsByUsername(String username) {
				return this.userRepository.existsByUsername(username);
		}

		public boolean existsByEmail(String email) {
				return this.userRepository.existsByEmail(email);
		}

		public void deleteById(Long id) {
				this.userRepository.deleteById(id);
		}

		public List<UserEntity> findAll() {
				return this.userRepository.findAll();
		}

		public Optional<UserEntity> findUserById(Long id) {
				return userRepository.findById(id);
		}

}
