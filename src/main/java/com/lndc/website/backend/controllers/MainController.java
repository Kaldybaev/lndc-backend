package com.lndc.website.backend.controllers;


import com.lndc.website.backend.controllers.utils.JwtResponse;
import com.lndc.website.backend.controllers.utils.LoginRequest;
import com.lndc.website.backend.controllers.utils.MessageResponse;
import com.lndc.website.backend.controllers.utils.SignupRequest;
import com.lndc.website.backend.models.ERole;
import com.lndc.website.backend.models.RoleEntity;
import com.lndc.website.backend.models.UserEntity;
import com.lndc.website.backend.securityconfig.JwtUtils;
import com.lndc.website.backend.services.RoleService;
import com.lndc.website.backend.services.UserDetailsImpl;
import com.lndc.website.backend.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.argon2.Argon2PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/auth")
public class MainController {

		@Autowired
		AuthenticationManager authenticationManager;

		@Autowired
		UserService userService;

		@Autowired
		RoleService roleService;

		@Autowired
		Argon2PasswordEncoder encoder;

		@Autowired
		JwtUtils jwtUtils;

		@PostMapping("/signin")
		public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {
				Authentication authentication = authenticationManager.authenticate(
						new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));

				SecurityContextHolder.getContext().setAuthentication(authentication);
				String jwt = jwtUtils.generateJwtToken(authentication);

				UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
				List<String> role = userDetails.getAuthorities().stream()
						.map(GrantedAuthority::getAuthority).collect(Collectors.toList());

				return ResponseEntity.ok(new JwtResponse(jwt,
						userDetails.getId(),
						userDetails.getUsername(),
						userDetails.getEmail(),
						role));
		}

		@PostMapping("/signup")
		public ResponseEntity<?> registerUser(@Valid @RequestBody SignupRequest signUpRequest) {
				if (userService.existsByUsername(signUpRequest.getUsername()))
						return ResponseEntity.badRequest().body(new MessageResponse("Error: Username is already taken!"));

				if (userService.existsByEmail(signUpRequest.getEmail()))
						return ResponseEntity.badRequest().body(new MessageResponse("Error: Email is already in use!"));

				UserEntity user = new UserEntity(signUpRequest.getUsername(), signUpRequest.getEmail(), encoder.encode(signUpRequest.getPassword()));

				String strrole = signUpRequest.getRole();
				RoleEntity role;

				String errorMessage = "Error: Role is not found.";

				if (strrole == null) {
						role = roleService.findByName(ERole.ROLE_USER).orElseThrow(() -> new RuntimeException(errorMessage));
				} else {
						switch (strrole) {
								case "admin":
										role = roleService.findByName(ERole.ROLE_ADMIN).orElseThrow(() -> new RuntimeException(errorMessage));
										break;
								case "spo":
										role = roleService.findByName(ERole.ROLE_SPONSOR).orElseThrow(() -> new RuntimeException(errorMessage));
										break;
								case "org":
										role = roleService.findByName(ERole.ROLE_ORGANISATOR).orElseThrow(() -> new RuntimeException(errorMessage));
										break;
								default:
										role = roleService.findByName(ERole.ROLE_USER).orElseThrow(() -> new RuntimeException(errorMessage));
						}
				}

				user.setURoleId(role);
				userService.save(user);

				return ResponseEntity.ok(new MessageResponse("User registered successfully!"));
		}
}
