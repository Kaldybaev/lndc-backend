package com.lndc.website.backend.controllers;

import com.lndc.website.backend.models.UserEntity;
import com.lndc.website.backend.repositories.UserRepository;
import com.lndc.website.backend.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api")
public class UserController {

		@Autowired
		UserRepository userRepository;

		@Autowired
		UserService userService;


		@GetMapping("/users")
		@PreAuthorize("hasRole('SPONSOR') or hasRole('ORGANISATOR') or hasRole('ADMIN')")
		public ResponseEntity<List<UserEntity>> getAllUsers() {
				try {
						List<UserEntity> userEntities = userService.findAll();
						if (userEntities.isEmpty()) return new ResponseEntity<>(HttpStatus.NO_CONTENT);
						return new ResponseEntity<>(userEntities, HttpStatus.OK);
				} catch (Exception e) {
						return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
				}
		}

		@GetMapping("/users/{id}")
		@PreAuthorize("hasRole('SPONSOR') or hasRole('ORGANISATOR') or hasRole('ADMIN')")
		public ResponseEntity<UserEntity> getUserById(@PathVariable("id") long id) {
				Optional<UserEntity> userEntities = userService.findUserById(id);
				return userEntities.map(userEntity ->
						new ResponseEntity<>(userEntity, HttpStatus.OK)).orElseGet(() ->
						new ResponseEntity<>(HttpStatus.NOT_FOUND));
		}


		@PostMapping("/users")
		@PreAuthorize("hasRole('USER') or hasRole('ORGANISATOR') or hasRole('ADMIN')")
		public ResponseEntity<UserEntity> createUser(@RequestBody UserEntity userEntity) {
				try {
						return new ResponseEntity<>(userRepository.save(userEntity), HttpStatus.CREATED);
				} catch (Exception e) {
						return new ResponseEntity<>(null, HttpStatus.EXPECTATION_FAILED);
				}
		}

		@PutMapping("/users/{id}")
		@PreAuthorize("hasRole('USER') or hasRole('ORGANISATOR') or hasRole('ADMIN')")
		public ResponseEntity<UserEntity> updateUser(@PathVariable("id") long id, @RequestBody UserEntity userEntity) {
				Optional<UserEntity> userEntities = userService.findUserById(id);
				if (userEntities.isPresent())
						return new ResponseEntity<>(userRepository.save(userEntity), HttpStatus.OK);
				else
						return new ResponseEntity<>(HttpStatus.NOT_FOUND);

		}

		@DeleteMapping("/users/{id}")
		@PreAuthorize("hasRole('ADMIN')")
		public ResponseEntity<HttpStatus> deleteUser(@PathVariable("id") long id) {
				try {
						userService.deleteById(id);
						return new ResponseEntity<>(HttpStatus.NO_CONTENT);
				} catch (Exception e) {
						return new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
				}
		}
}
