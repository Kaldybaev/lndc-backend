package com.lndc.website.backend.repositories;

import com.lndc.website.backend.models.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<UserEntity, Long> {
		UserEntity findByEmail(String email);

		Optional<UserEntity> findById(Long id);

		Optional<UserEntity> findByUsername(String userName);

		void deleteById(Long id);

		Boolean existsByUsername(String username);

		Boolean existsByEmail(String email);
}
