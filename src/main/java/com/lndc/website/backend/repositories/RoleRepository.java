package com.lndc.website.backend.repositories;

import com.lndc.website.backend.models.ERole;
import com.lndc.website.backend.models.RoleEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RoleRepository extends JpaRepository<RoleEntity, Integer> {

		Optional<RoleEntity> findByName(ERole name);

		RoleEntity findById(long id);

		RoleEntity findRoleByName(ERole name);

}
