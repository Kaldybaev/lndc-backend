package com.lndc.website.backend.securityconfig;

import com.lndc.website.backend.services.UserDetailsServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.argon2.Argon2PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import java.util.Collections;

@Configuration
@EnableWebSecurity
public class WebSecurityConfiguration extends WebSecurityConfigurerAdapter {

		@Autowired
		UserDetailsServiceImpl userDetailsService;

		@Autowired
		private AuthEntryPointJwt unauthorizedHandler;

		@Bean
		public Argon2PasswordEncoder passwordEncoder() {
				return new Argon2PasswordEncoder();
		}

		@Autowired
		public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
				auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
		}

		@Bean
		@Override
		public AuthenticationManager authenticationManagerBean() throws Exception {
				return super.authenticationManagerBean();
		}

		@Bean
		public AuthTokenFilter authenticationJwtTokenFilter() {
				return new AuthTokenFilter();
		}

		@Override
		protected void configure(HttpSecurity http) throws Exception {
				http.cors().and().csrf().disable()
						.exceptionHandling().authenticationEntryPoint(unauthorizedHandler).and()
						.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
						.cors().configurationSource(corsConfigurationSource()).and()
						.authorizeRequests().antMatchers("/auth/**").permitAll()
						.antMatchers("/api/**").permitAll()
						.anyRequest().authenticated();

				http.addFilterBefore(authenticationJwtTokenFilter(), UsernamePasswordAuthenticationFilter.class);
		}

		@Bean
		CorsConfigurationSource corsConfigurationSource() {
				CorsConfiguration configuration = new CorsConfiguration();
				configuration.setAllowedOrigins(Collections.singletonList("*"));
				configuration.setAllowedMethods(Collections.singletonList("*"));
				UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
				source.registerCorsConfiguration("/**", configuration);
				return source;
		}
}
